var CURRENT_USER = "";

<!--User tab-->
var hasInitCategoriesList = false;
$(document).ready(function(){
	fillCountrySelectBox();
	//init tabs
	$('#tabs-topnav li:eq(1) a').on('shown.bs.tab', function (e) {
		clearUserForm();
		//user tab
		initUsersTable();
	});
	$('#tabs-topnav li:eq(2) a').on('shown.bs.tab', function (e) {
		//category tab
		initCategoriesTable();
		clearCategoryForm();
	});
	$('#tabs-topnav li:eq(3) a').on('shown.bs.tab', function (e) {
		clearBeerForm();
		if(!hasInitCategoriesList){
			$.ajax({
			    type: "GET",
			    url: BASE_REST_URL + '/category/',
			    //contentType: "application/json",
			    success: function(data){
			    	localCategoriesList = data;
			    	hasInitCategoriesList = true;
			    },
			    error: function(ret){showError(ret);}
			});
		}
		//beer tab
		initBeersTable();
	});
});
//load users
var localUsersList = [];
function initUsersTable(){
	$('#table-users').bootstrapTable('refresh', {
		url: BASE_REST_URL + '/man/user/'
    })
    .on('load-success.bs.table', function (e, data) {
    	//if(console)console.log('Event: dbl-click-row.bs.table, data: ' + JSON.stringify(data));
    	//update local list
    	localUsersList = data;
    });
}

function userStatusFormatter(value, row, index) {
	var check = row.enabled==1 ? "checked=checked " : "";
	var disabled = "disabled=disabled ";
    return "<input type=checkbox " + check + " " + disabled + " " + "/>";
}

function userActionFormatter(value, row, index) {
	var btnDelete = "";
	var btnToggle = "";
	var toggleLabel = row.enabled==1 ? "Disable" : "Enable";
	if(row.username!=CURRENT_USER){
		btnDelete ='<button type="button" class="btn btn-default btn-sm" onclick=deleteUser("'+row.username+'")><span class="glyphicon glyphicon-trash"></span> Delete </button>';
		btnToggle ='<button type="button" class="btn btn-default btn-sm" onclick=toggleUserStatus("'+row.username+'","'+row.enabled+'")><span class="glyphicon glyphicon-lock"></span> ' + toggleLabel +' </button>';
	}
    return btnToggle + btnDelete;
}

//check if available
function checkUsernameAvailable(){
	return true;
}		

//enable/disable another user
function toggleUserStatus(username,enabled){
	var confirmMsg = enabled==1 ? "Disable this user?" : "Enable this user?"
	if(confirm(confirmMsg)){
		var u = BASE_REST_URL + "/man/user/";
		var d = {
			"username": username,
			"enabled" : enabled==1 ? 2 : 1
		}
		doRestCall(u, JSON.stringify(d), "PATCH", function(){showInfo("Toggled user stattus successfully.");initUsersTable();});
	}
}

//change password of the current user
function changePassword(){
	if(!checkPasswordMatch()){
		showWarning("Passwords not match.");
		return;
	}
	var currentPass = $("#pwd-old").val();
	var newPass = $("#pwd-new").val();
	var u = BASE_REST_URL + "/man/user/" + CURRENT_USER + "/password?currentpass=" + currentPass + "&changedpass=" + newPass;
	var d = {}
	doRestCall(u, JSON.stringify(d), "PUT", function(){showInfo("Password changed.");});
}

function checkPasswordMatch(){
	return $("#pwd-new").val() == $("#pwd-new2").val();
}

//add new user
function addUser(){
	var u = BASE_REST_URL + "/man/user/";
	var d = {
		"username": $("#form-user #usr").val(),
		"password": $("#form-user #pwd").val(),
		"enabled" : $("#form-user #chkEnabled")[0].checked ? 1 : 2
	}
	doRestCall(u, JSON.stringify(d), "POST", addUserCallback);
}
function addUserCallback(){
	showInfo("User was created.");
	clearUserForm();
	initUsersTable();
}
function clearUserForm(){
	$("#form-user #usr").val("");
	$("#form-user #pwd").val("");
	$("#form-user #chkEnabled").attr("checked",false);
}
//delete a user
function deleteUser(username){
	if(confirm("Confirm to delete this user?")){
		var u = BASE_REST_URL + "/man/user/" + username;
		var d = {}
		doRestCall(u, JSON.stringify(d), "DELETE", deleteUserCallback);
	}
}

function deleteUserCallback(){
	showInfo("Deleted user successfully.");
	initUsersTable();
}

<!--Category tab-->
//load categories
var localCategoriesList = [];
function initCategoriesTable(){
	$('#table-categories').bootstrapTable('refresh', {
		url: BASE_REST_URL + '/category'
    }).on('dbl-click-row.bs.table', function (e, row, $element) {
       //if(console)console.log('Event: dbl-click-row.bs.table, data: ' + JSON.stringify(row));
       selectCategory(row);
    })
    .on('load-success.bs.table', function (e, data) {
    	//if(console)console.log('Event: dbl-click-row.bs.table, data: ' + JSON.stringify(data));
    	//update local list
    	localCategoriesList = data;
    	hasInitCategoriesList = true;
    });
}

function fillCategorySelectBox(){
	var sel = document.getElementById("beer-category");
	while (sel.options.length) {
        sel.remove(0);
    }
    if (localCategoriesList) {
        var i;
        for (i = 0; i < localCategoriesList.length; i++) {
            var opt = new Option(localCategoriesList[i].catName, localCategoriesList[i].id+"");
            sel.options.add(opt);
        }
    }
}

function fillCountrySelectBox(){
	var sel = document.getElementById("beer-country");
	for (var i = 0; i < countries.length; i++) {
        var opt = new Option(countries[i].name, countries[i].code);
        sel.options.add(opt);
    }
}

function clearCategoryForm(){
	$("#form-category #category-id").val("");
	$("#form-category #category-name").val("");
	categoryRestMode = "POST";
}
function selectCategory(row){
	$("#form-category #category-id").val(row.id);
	$("#form-category #category-name").val(row.catName);
	$("#form-category #category-name").focus();
	categoryRestMode = "PATCH";
}

function categoryActionFormatter(value, row, index) {
    return '<button type="button" class="btn btn-default btn-sm" onclick=deleteCategory("'+row.id+'")><span class="glyphicon glyphicon-trash"></span> Delete </button>';
}

//check if available
function checkCategoryAvailable(){
	return true;
}

var categoryRestMode = "POST";
//add new category
function saveCategory(){
	var u = BASE_REST_URL + "/man/category/";
	var d = {
		"id": $("#form-category #category-id").val(),
		"catName": $("#form-category #category-name").val()
	}
	doRestCall(u, JSON.stringify(d), categoryRestMode, saveCategoryCallback);
}

function saveCategoryCallback(){
	showInfo("Category was saved.");
	clearCategoryForm();
	initCategoriesTable();
}

//delete a category
function deleteCategory(id){
	if(confirm("Confirm to delete this category?")){
		var u = BASE_REST_URL + "/man/category/" + id;
		var d = {}
		doRestCall(u, JSON.stringify(d), "DELETE", deleteCategoryCallback);
	}
}
function deleteCategoryCallback(){
	showInfo("Category was deleted.");
	initCategoriesTable();
}

<!--Beer tab-->
//load beers
var localBeersList = [];
function initBeersTable(){
	$('#table-beers').bootstrapTable('refresh', {
		url: BASE_REST_URL + '/man/beer'
    }).on('dbl-click-row.bs.table', function (e, row, $element) {
       //if(console)console.log('Event: dbl-click-row.bs.table, data: ' + JSON.stringify(row));
       selectBeer(row);
    })
    .on('load-success.bs.table', function (e, data) {
    	//if(console)console.log('Event: dbl-click-row.bs.table, data: ' + JSON.stringify(data));
    	//update local list
    	localBeersList = data;
    	//refresh category select
    	fillCategorySelectBox();
    });
}

function beerStatusFormatter(value, row, index) {
	var check = row.status==1 ? "checked=checked " : "";
	var disabled = "disabled=disabled ";
    return "<input type=checkbox " + check + " " + disabled + " " + "/>";
}

function beerCategoryFormatter(value, row, index) {
    return row.category.catName;
}

function beerCountryFormatter(value, row, index) {
	if(row.country.length>0){
		var c = findInListByProperty(countries, "code", row.country);
		if(c)return c.name;
	}
    return "";
}

function checkValidPrice(){
	var p = $("#beer-price").val();
	if(isNaN(p) || parseFloat(p)<0){
		$("#beer-price").val("0");
		$("#beer-price").focus();
	}
}

function clearBeerForm(){
	$("#form-beer #beer-id").val("");
	$("#form-beer #beer-name").val("");
	$("#form-beer #beer-desc").val("");
	$("#form-beer #beer-manufactor").val("");
	//$("#form-beer #beer-country").val("");
	$("#form-beer #beer-price").val("");
	//document.getElementById("beer-category").selectedIndex = 0;
	$("#form-beer #beer-status").val(1);
	beerRestMode = "POST";
}
function selectBeer(row){
	$("#form-beer #beer-id").val(row.id);
	$("#form-beer #beer-name").val(row.beerName);
	$("#form-beer #beer-name").focus();
	$("#form-beer #beer-desc").val(row.description);
	$("#form-beer #beer-manufactor").val(row.manufactor);
	var selCountry = document.getElementById("beer-country");
	for(var i=0;i<selCountry.options.length;i++){
		if(selCountry.options[i].value==(row.country+"")){
			selCountry.selectedIndex = i;
			selCountry.options[i].selected = true;
			selCountry.value = selCountry.options[i].value;
			break;
		}
	}
	$("#form-beer #beer-price").val(row.price);
	var sel = document.getElementById("beer-category");
	for(var i=0;i<sel.options.length;i++){
		if(sel.options[i].value==(row.category.id+"")){
			sel.selectedIndex = i;
			sel.options[i].selected = true;
			sel.value = sel.options[i].value;
			break;
		}
	}
	$("#form-beer #beer-status").val(row.status);
	beerRestMode = "PUT";
}

function beerActionFormatter(value, row, index) {
	var available = row.status==1;
	var toggleLabel = available ? "Archive" : "Unarchive";
	var btnToggle ='<button type="button" class="btn btn-default btn-sm" onclick=toggleBeerStatus("'+row.id+'","'+row.status+'")><span class="glyphicon glyphicon-lock"></span> ' + toggleLabel +' </button>';
    var btnDelete = '<button type="button" class="btn btn-default btn-sm" onclick=deleteBeer("'+row.id+'")><span class="glyphicon glyphicon-trash"></span> Delete </button>';
    return btnToggle + btnDelete;
}

//check if available
function checkBeerAvailable(){
	return true;
}

var beerRestMode = "POST";
//add new beer
function saveBeer(){
	var u = BASE_REST_URL + "/man/beer/";
	var d = {
		"id": $("#form-beer #beer-id").val(),
		"category": {"id" : document.getElementById("beer-category").value},
		"beerName": $("#form-beer #beer-name").val(),
		"description" : $("#form-beer #beer-desc").val(),
		"manufactor" : $("#form-beer #beer-manufactor").val(),
		"country" : $("#form-beer #beer-country").val(),
		"price" : $("#form-beer #beer-price").val(),
		"status" :$("#form-beer #beer-status").val(),
	}
	doRestCall(u, JSON.stringify(d), beerRestMode, saveBeerCallback);
}

function saveBeerCallback(){
	showInfo("Beer was saved.");
	clearBeerForm();
	initBeersTable();
}

//delete a beer
function deleteBeer(id){
	if(confirm("Confirm to delete this beer?")){
		var u = BASE_REST_URL + "/man/beer/" + id;
		var d = {}
		doRestCall(u, JSON.stringify(d), "DELETE", deleteBeerCallback);
	}
}
function deleteBeerCallback(){
	showInfo("Beer was deleted.");
	initBeersTable();
}

//enable/disable a beer
function toggleBeerStatus(id,status){
	var confirmMsg = status==1 ? "Archive this beer?" : "Unarchive this beer?"
	if(confirm(confirmMsg)){
		var u = BASE_REST_URL + "/man/archive/beer/";
		var d = {
			"id": id,
			"status" : status==1 ? 2 : 1
		}
		doRestCall(u, JSON.stringify(d), "PATCH", function(){showInfo("Toggled beer stattus successfully.");initBeersTable();});
	}
}