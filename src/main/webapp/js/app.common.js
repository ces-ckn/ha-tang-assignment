var BASE_REST_URL = "/";

var csrf_token = null;
var csrf_header = null;
$body = $("body");

$(document).on({
    ajaxStart: function() { 
    	$body.addClass("loading");
    	$(".modal_overlay").show();
    	$(".btn").attr("disabled",true);
    	hideAllMsgBoxes();
    },
    ajaxStop: function() {
    	$(".modal_overlay").hide();
    	$body.removeClass("loading"); 
    	$(".btn").attr("disabled",false);
    }    
});

$(document).ready(function(){
	$('[data-toggle="popover"]').popover();
	csrf_token = $("meta[name='_csrf']").attr("content");
	csrf_header = $("meta[name='_csrf_header']").attr("content");
});

function doRestCall(url, jsonData, method, callback, contentType){
	if(typeof contentType=="undefined"){contentType = "application/json";}
	if( csrf_token && csrf_header){
		var head = [];
		head[csrf_header] = csrf_token;
		console.log(head);
		$.ajax({
			headers: head,
		    type: method,
		    url: url,
		    contentType: contentType,
		    data: jsonData,
		    success: callback,
		    error: function(ret){showError(ret);}
		});
	}
	else{
		$.ajax({
		    type: method,
		    url: url,
		    contentType: contentType,
		    data: jsonData,
		    success: callback,
		    error: function(ret){showError(ret);}
		});
	}
}

function showError(data){
	if(console)console.log(data);
	if(typeof exampleCaptcha != "undefined")exampleCaptcha.ReloadImage();
	showWarning($.parseJSON(data.responseText).errorMessage);
}

function showInfo(msg){
	$(".alert-info span").html(msg);
	$(".alert-info").css("display","");
}
function hideInfo(){
	$(".alert-info").css("display","none");
}
function showWarning(msg){
	$(".alert-warning span").html(msg);
	$(".alert-warning").css("display","");
}
function hideWarning(){
	$(".alert-warning").css("display","none");
}
function hideAllMsgBoxes(){
	hideInfo();
	hideWarning();
}

function findInListByProperty(list, propName, val){
	for (var i = 0; i < list.length; i++){
		if(list[i][propName]==val)return list[i];
	}
	return null;
}

function doSignup(){
	if(!checkPaswordMatch()){
		if(typeof exampleCaptcha != "undefined")exampleCaptcha.ReloadImage();
		return;
	}
	var data = $('#form-signup').serialize();
	doRestCall($('#form-signup').attr("action"), data, "POST", signupCallback,"application/x-www-form-urlencoded");
}

function signupCallback(){
	showInfo("User created successfully.");
	$("#cust-pwd").val("");
	$("#cust-pwd2").val("");
	$("#cust-email").val("");
	if(typeof exampleCaptcha != "undefined")exampleCaptcha.ReloadImage();
}