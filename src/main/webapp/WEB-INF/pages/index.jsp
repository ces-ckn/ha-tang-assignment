<%@page import="botdetect.web.Captcha"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="/" var="appBaseUrl" />
<html>
<head>
	<meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="${appBaseUrl}js/app.common.js"></script>
    <style>
		.modal_overlay {
		    display:    none;
		    position:   fixed;
		    z-index:    1000;
		    top:        0;
		    left:       0;
		    height:     100%;
		    width:      100%;
		    background: rgba( 255, 255, 255, .2 ) 
		                url('${appBaseUrl}images/ajax-spinner.gif') 
		                50% 50% 
		                no-repeat;
		}
		
		/* When the body has the loading class, we turn
		   the scrollbar off with overflow:hidden */
		body.loading {
		    overflow: hidden;   
		}
		
		/* Anytime the body has the loading class, our
		   modal element will be visible */
		body.loading .modal {
		    display: block;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Beverage app</a>
			</div>
			<div>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="admin"><span class="glyphicon glyphicon-th-list"></span>  Admin panel</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<h2>Welcome to Calgary's Craft Beer World!</h2>
		<p><!-- spacing --></p>
		
		<!-- message -->
		<div class="alert alert-info" style="display: none">
			<a href="#" class="close" onclick="hideInfo();return false;">&times;</a>
			<strong>Info!</strong> <span></span>
		</div>
		<div class="alert alert-warning" style="display: none">
			<a href="#" class="close" onclick="hideWarning();return false;">&times;</a>
			<strong>Warning!</strong> <span></span>
		</div>
		
		<h4>Sign up for a Customer passport</h4>
		<form id="form-signup" class="form-horizontal" role="form" onsubmit="doSignup();return false;"
			action="${appBaseUrl}signup" method="POST">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			<div class="form-group">
				<label class="control-label col-sm-2" for="custemail">Enter your email:</label>
				<div class="col-sm-4">
					<input type="email" required="required" class="form-control"  id="cust-email" name="custemail"
						placeholder="Email">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="custpassword">Choose a password:</label>
				<div class="col-sm-4">
					<input type="password" required="required" class="form-control" id="cust-pwd" name="custpassword"
						placeholder="New password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="cust-pwd2">Confirm password:</label>
				<div class="col-sm-4">
					<input type="password" required="required" class="form-control" id="cust-pwd2"
						placeholder="Confirm password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="captchaCodeTextBox"></label>
				<div class="col-sm-4">
					<%
						Captcha captcha = Captcha.load(request, "exampleCaptcha");
						captcha.renderCaptchaMarkup(pageContext.getServletContext(),
								pageContext.getOut());
					%>
					<input id="captchaCodeTextBox" type="text" class="form-control" name="captchaCodeTextBox" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-4">
					<button type="submit" class="btn btn-default" id="btnSignup">Sign up</button>
				</div>
			</div>
		</form>
	</div>
	
	<script type="text/javascript">
		function checkPaswordMatch(){
			var matched = $("#cust-pwd").val()==$("#cust-pwd2").val();
			if(!matched){showWarning("Passwords not match.");}
			return matched;
		}
		
		if("${msg}".length>0){
			showInfo("${msg}");
		}
		if("${err}".length>0){
			showWarning("${err}");
		}
	</script>
	<div class="modal_overlay"><!-- loading overlay --></div>
</body>
</html>
