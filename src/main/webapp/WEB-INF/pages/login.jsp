<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

</head>
<body onload='document.loginForm.username.focus();'>

	<div class="container">
		<h2>Login Form</h2>
		<p>Login with Username and Password</p>

		<form name='loginForm' action="<c:url value='/login' />"
			method='POST' class="form-horizontal" role="form">
			<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
			<div class="form-group">
				<label class="control-label col-sm-2" for="username">Username:</label>
				<div class="col-sm-4">
					<input type="text" required="required" class="form-control" id="username" name="username"
						placeholder="Username">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="password">Password:</label>
				<div class="col-sm-4">
					<input type="password" required="required" class="form-control" id="password" name="password"
						placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-4">
					<div id="btnGroupCategorySaveCancel" class="btn-group">
						<button type="submit" class="btn btn-default">Login</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>