<!DOCTYPE html>
<html lang="en">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<c:url value="/" var="appBaseUrl" />
<head>
	<title>Beverage App</title>
	<meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet"
		href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet"
		href="${appBaseUrl}css/bootstrap-table.css">		
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script
		src="${appBaseUrl}js/bootstrap-table.min.js"></script>
	<script src="${appBaseUrl}js/app.common.js"></script>	
	<script src="${appBaseUrl}js/countries.js"></script>	
	<script src="${appBaseUrl}js/man/app.admin.js"></script>
	<style>
		.modal_overlay {
		    display:    none;
		    position:   fixed;
		    z-index:    1000;
		    top:        0;
		    left:       0;
		    height:     100%;
		    width:      100%;
		    background: rgba( 255, 255, 255, .2 ) 
		                url('${appBaseUrl}images/ajax-spinner.gif') 
		                50% 50% 
		                no-repeat;
		}
		
		/* When the body has the loading class, we turn
		   the scrollbar off with overflow:hidden */
		body.loading {
		    overflow: hidden;   
		}
		
		/* Anytime the body has the loading class, our
		   modal element will be visible */
		body.loading .modal {
		    display: block;
		}
	</style>

			
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Admin panel</a>
			</div>
			<div>
				<c:if test="${pageContext.request.userPrincipal.name != null}">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" style="cursor:default"><span class="glyphicon glyphicon-user"></span>  ${pageContext.request.userPrincipal.name}</a></li>
					<li><a href="javascript:formSubmit()"><span class="glyphicon glyphicon-log-in"></span>  Log out</a></li>
				</ul>
				</c:if>
			</div>
		</div>
	</nav>
	<div class="container">
		<!-- message -->
		<div class="alert alert-info" style="display: none">
			<a href="#" class="close" onclick="hideInfo();return false;">&times;</a>
			<strong>Info!</strong> <span></span>
		</div>
		<div class="alert alert-warning" style="display: none">
			<a href="#" class="close" onclick="hideWarning();return false;">&times;</a>
			<strong>Warning!</strong> <span></span>
		</div>

		<!-- tabs -->
		<ul class="nav nav-tabs" id="tabs-topnav">
			<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
			<li><a data-toggle="tab" href="#menu1">Admin user</a></li>
			<li><a data-toggle="tab" href="#menu2">Category</a></li>
			<li><a data-toggle="tab" href="#menu3">Beer</a></li>
		</ul>

		<div class="tab-content">
			<div id="home" class="tab-pane fade in active">
				<h3>HOME</h3>
				<p>Management panel</p>
				<ul class="">
					<li>Admin user: manage admin users.</li>
					<li>Category: mange beer categories. Categories must exist to
						be able to add a new beer type.
						<br/>
						Public API: <a href="${appBaseUrl}rest/category" target="new">/rest/category</a>	
					</li>
					<li>Beer: manage beer types.
						<br/>
						Public API: <a href="${appBaseUrl}rest/beer" target="new">/rest/beer</a>
						<br/>
						Protected API: <a href="${appBaseUrl}rest/customer/beers" target="new">/rest/customer/beers</a>	
					</li>
					<li>Details for API: <a href="${appBaseUrl}public/API.txt" target="new">REST resources</a>
					</li>
				</ul>
				
				<h4>Change password</h4>
				<form id="form-changepass" class="form-horizontal" role="form" onsubmit="changePassword();return false;">
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd-old">Current password:</label>
						<div class="col-sm-4">
							<input type="password" required="required" class="form-control" id="pwd-old"
								placeholder="Currnet password">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd-new">New password:</label>
						<div class="col-sm-4">
							<input type="password" required="required" class="form-control" id="pwd-new"
								placeholder="New password">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd-new2">Confirm password:</label>
						<div class="col-sm-4">
							<input type="password" required="required" class="form-control" id="pwd-new2"
								placeholder="Confirm password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-4">
							<button type="submit" class="btn btn-default" id="btnSavePassword">Save</button>
						</div>
					</div>
				</form>
			</div>
			<div id="menu1" class="tab-pane fade">
				<h3>Manage administrative user</h3>
				<p><!-- spacing --></p>
				<form id="form-user" class="form-horizontal" role="form" onsubmit="addUser();return false;">
					<div class="form-group">
						<label class="control-label col-sm-2" for="usr">Username:</label>
						<div class="col-sm-4">
							<input type="text" required="required" class="form-control" id="usr"
								placeholder="Enter username">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd">Password:</label>
						<div class="col-sm-4">
							<input type="password" required="required" class="form-control" id="pwd"
								placeholder="Enter password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-4">
							<div class="checkbox">
								<label><input type="checkbox" id="chkEnabled"> Enabled</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-4">
							<button type="submit" class="btn btn-default" id="btnAddUser">Add</button>
						</div>
					</div>
				</form>
				<!-- users table -->
				<table id="table-users"
					data-toggle="table"
					data-pagination="true" data-search="true" 
					data-single-select="true"
					class="table table-striped">
					<thead>
						<tr>
							<th data-field="username" class="col-md-6" data-align="left" data-sortable="true">Username</th>
							<th data-field="enabled"  class="col-md-2" data-sortable="false" data-align="center"
								data-formatter="userStatusFormatter">Enabled</th>
							<th class="col-md-2" data-sortable="false" data-align="center"
								data-formatter="userActionFormatter"></th>	
						</tr>
					</thead>
				</table>
			</div>
			<div id="menu2" class="tab-pane fade">
				<h3>Manage category</h3>
				<form id="form-category" class="form-horizontal" role="form" onsubmit="saveCategory();return false;">
					<div class="form-group">
						<label class="control-label col-sm-2" for="category-name">Category name:</label>
						<div class="col-sm-4">
							<input type="hidden" id="category-id" />
							<input type="text" required="required" class="form-control" id="category-name"
								placeholder="Enter a name">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-4">
							<div id="btnGroupCategorySaveCancel" class="btn-group">
								<button type="submit" class="btn btn-default">Save</button>
								<button type="button" class="btn btn-default" onclick="clearCategoryForm();return false;">Cancel</button>
							</div>
						</div>
					</div>
				</form>
				<!-- categories table -->
				<table id="table-categories"
					data-toggle="table"
					data-pagination="true" data-search="true" 
					data-single-select="true"
					class="table table-striped">
					<thead>
						<tr>
							<th data-field="catName" class="col-md-6" data-align="left" data-sortable="true">Category name</th>
							<th class="col-md-2" data-sortable="false" data-align="center"
								data-formatter="categoryActionFormatter"></th>	
						</tr>
					</thead>
				</table>
			</div>
			<div id="menu3" class="tab-pane fade">
				<h3>Manage beer</h3>
				<form id="form-beer" class="form-horizontal" role="form" onsubmit="saveBeer();return false;">
					<div class="form-group">
						<label class="control-label col-sm-2" for="beer-name">Beer name:</label>
						<div class="col-sm-4">
							<input type="hidden" id="beer-id" />
							<input type="hidden" id="beer-status" />
							<input type="text" required="required" class="form-control" id="beer-name"
								placeholder="Enter a name">
						</div>
						<label class="control-label col-sm-2" for="beer-desc">Description:</label>
						<div class="col-sm-4">
							<textarea class="form-control" id="beer-desc" rows="2"
								placeholder="Enter description"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="beer-manufactor">Manufacturer:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="beer-manufactor"
								placeholder="Enter manufacturer">
						</div>	
						<label class="control-label col-sm-2" for="beer-country">Country:</label>
						<div class="col-sm-4">
							<select class="form-control" id="beer-country">
							</select>	
						</div>
					</div>
					<div class="form-group">	
						<label class="control-label col-sm-2" for="beer-price">Price:</label>
						<div class="col-sm-4">
							<input type="text" min="0" class="form-control" id="beer-price"
								title="Price" data-toggle="popover" data-trigger="hover" 
								data-placement="bottom" data-content="Not less than 0"
								placeholder="Enter price" onblur="checkValidPrice();">
						</div>
						<label class="control-label col-sm-2" for="beer-category">Category:</label>
						<div class="col-sm-4"> 
							<select class="form-control" id="beer-category">
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-4">
							<div id="btnGroupBeerSaveCancel" class="btn-group">
								<button type="submit" class="btn btn-default">Save</button>
								<button type="button" class="btn btn-default" onclick="clearBeerForm();return false;">Cancel</button>
							</div>
						</div>
					</div>
				</form>
				<!-- beer table -->
				<table id="table-beers" 
					data-toggle="table"
					data-pagination="true" data-search="true" 
					data-single-select="true"
					class="table table-striped">
					<thead>
						<tr>
							<th data-field="beerName" class="col-md-2" data-align="left" data-sortable="true">Beer name</th>
							<th data-field="description" class="col-md-2" data-align="left" data-sortable="true">Description</th>
							<th data-field="manufactor" class="col-md-2" data-align="left" data-sortable="true">Manufacturer</th>
							<th data-field="country" class="col-md-1" data-align="left" data-sortable="true" data-formatter="beerCountryFormatter">Country</th>
							<th data-field="price" class="col-md-1" data-align="left" data-sortable="true">Price</th>
							<th data-field="category" class="col-md-1" data-align="left" data-sortable="true" data-formatter="beerCategoryFormatter">Category</th>
							
							<th data-field="status"  class="col-md-1" data-sortable="false" data-align="center"
								data-formatter="beerStatusFormatter">Available</th>
							<th class="col-md-2" data-sortable="false" data-align="center"
								data-formatter="beerActionFormatter"></th>	
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	

	<script>
		<!--Common functions-->
		
		BASE_REST_URL = "${appBaseUrl}rest";
		CURRENT_USER = "${pageContext.request.userPrincipal.name}";
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>
	
	<form action="${appBaseUrl}logout" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<div class="modal_overlay"><!-- loading overlay --></div>
</body>
</html>
