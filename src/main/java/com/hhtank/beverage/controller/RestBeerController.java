package com.hhtank.beverage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hhtank.beverage.domain.Beer;
import com.hhtank.beverage.enums.EnumItemStatus;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.exception.TransisentPropertyException;
import com.hhtank.beverage.service.BeerService;
import com.hhtank.beverage.utils.RestResult;

/**
 * RESTful Controller for beer management
 *
 * @author hhtank
 */
@RestController
public class RestBeerController {

    @Autowired
    BeerService beerService;

    /**
     * Archive a beer then it will no long be served
     *
     * @param inputBeer input beer
     * @return archive result
     */
    @SuppressWarnings("boxing")
    @RequestMapping(value = "/rest/man/archive/beer", method = RequestMethod.PATCH, headers = "Accept=application/json")
    public ResponseEntity<RestResult> archive(@RequestBody final Beer inputBeer) {
        RestResult result = new RestResult();
        try {
            Beer patching = new Beer();
            patching.setId(inputBeer.getId());
            patching.setStatus(inputBeer.getStatus());
            beerService.mergeOnly(patching);
            result.setDataObject(true);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (ItemNotFoundException | TransisentPropertyException e) {
            result.setErrorMessage("Requested item was not found");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Create a new beer
     *
     * @param inputBeer input beer
     * @return creation result
     */
    @RequestMapping(value = "/rest/man/beer", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<RestResult> create(@RequestBody final Beer inputBeer) {
        RestResult result = new RestResult();
        try {
            if (inputBeer.getPrice() < 0) {
                result.setErrorMessage("Invalid price");
                return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
            }
            inputBeer.setStatus(EnumItemStatus.AVAILABLE.getValue());
            Beer beer = beerService.create(inputBeer);
            result.setDataObject(beer);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e) {
            result.setErrorMessage("Beer name existed.");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (TransisentPropertyException e) {
            result.setErrorMessage("Requested item was not found");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Delete a beer
     *
     * @param id deleting beer's id
     * @return deleting result
     */
    @SuppressWarnings("boxing")
    @RequestMapping(value = "/rest/man/beer/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ResponseEntity<RestResult> delete(@PathVariable final int id) {
        RestResult result = new RestResult();
        try {
            Beer beer = new Beer();
            beer.setId(id);
            beerService.delete(beer);
            result.setDataObject(true);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (ItemNotFoundException e) {
            result.setErrorMessage("Requested item was not found");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Return a list of beer in management scope
     *
     * @return list of beer
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/rest/man/beer", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity getAll() {
        RestResult result = new RestResult();
        try {
            List<Beer> beers = beerService.findAll();
            return new ResponseEntity(beers, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Return a list of beer in public scope
     *
     * @return list of beers
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/rest/beer", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity getAllPublic() {

        RestResult result = new RestResult();
        try {
            List<Beer> beers = beerService.findPublic();
            return new ResponseEntity(beers, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Get details of a beer in management scope
     *
     * @param id beer id
     * @return Beer
     */
    @RequestMapping(value = "/rest/man/beer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<RestResult> getDetails(@PathVariable int id) {
        RestResult result = new RestResult();
        try {
            Beer beer = beerService.findById(id);
            result.setDataObject(beer);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Get details of a beer in public scope
     *
     * @param id beer id
     * @return Beer
     */
    @RequestMapping(value = "/rest/beer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<RestResult> getDetailsPublic(@PathVariable final int id) {

        RestResult result = new RestResult();
        try {
            Beer beer = beerService.findPublic(id);
            result.setDataObject(beer);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Update beer details
     *
     * @param inputBeer input beer
     * @return update result
     */
    @RequestMapping(value = "/rest/man/beer", method = RequestMethod.PUT, headers = "Accept=application/json")
    public ResponseEntity<RestResult> replace(@RequestBody final Beer inputBeer) {
        RestResult result = new RestResult();
        try {
            if (inputBeer.getPrice() < 0) {
                result.setErrorMessage("Invalid price");
                return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
            }
            Beer beer = beerService.update(inputBeer);
            result.setDataObject(beer);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (ItemNotFoundException | TransisentPropertyException e) {
            result.setErrorMessage("Requested item was not found");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }
}