package com.hhtank.beverage.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import botdetect.web.Captcha;

import com.hhtank.beverage.domain.User;
import com.hhtank.beverage.domain.UserRole;
import com.hhtank.beverage.enums.EnumRole;
import com.hhtank.beverage.exception.DuplicatedObjectException;
import com.hhtank.beverage.service.UserService;
import com.hhtank.beverage.utils.CommonUtils;
import com.hhtank.beverage.utils.RestResult;

/**
 * Base controller
 *
 * @author hhtank
 */
@Controller
public class BaseController {

    private final static org.slf4j.Logger logger     = LoggerFactory.getLogger(BaseController.class);

    private static final String           VIEW_403   = "403";
    private static final String           VIEW_ADMIN = "admin";
    private static final String           VIEW_INDEX = "index";
    private static final String           VIEW_LOGIN = "login";
    @Autowired
    UserService                           userService;

    /**
     * @return view for 403 error
     */
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accessDenied() {
        logger.debug("[admin]");
        return VIEW_403;
    }

    /**
     * @return admin view
     */
    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public String admin() {
        logger.debug("[admin]");
        return VIEW_ADMIN;
    }

    /**
     * @return login view
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        logger.debug("[admin]");
        return VIEW_LOGIN;
    }

    /**
     * @param custemail registering email
     * @param custpassword registering password
     * @param request current requeest
     * @return signup result
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = { "/signup" }, method = RequestMethod.POST)
    public ResponseEntity signup(@RequestParam String custemail, @RequestParam String custpassword, HttpServletRequest request) {
        RestResult result = new RestResult();
        Captcha captcha = Captcha.load(request, "exampleCaptcha");
        boolean isHuman = captcha.validate(request, request.getParameter("captchaCodeTextBox"));
        if (!isHuman) {
            result.setErrorMessage("Wrong verification code.");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }

        logger.debug("[welcome]");
        // Spring uses InternalResourceViewResolver and return back index.jsp
        User u = new User(custemail, CommonUtils.hashingPassword(custpassword));
        UserRole ur = new UserRole(u, EnumRole.ROLE_CUTOMER.getValue());
        u.getUserRoles().add(ur);
        try {
            u = userService.create(u);
        }
        catch (DuplicatedObjectException e) {
            e.printStackTrace();
            result.setErrorMessage("User exisited.");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);

        }
        catch (Exception e) {
            e.printStackTrace();
            result.setErrorMessage("Error occur, please try again later!");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(VIEW_INDEX, HttpStatus.OK);
    }

    /**
     * @return default view
     */
    @RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
    public String welcome() {
        logger.debug("[welcome]");
        // Spring uses InternalResourceViewResolver and return back index.jsp
        return VIEW_INDEX;
    }

}