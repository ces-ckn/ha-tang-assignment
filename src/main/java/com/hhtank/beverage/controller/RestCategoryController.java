package com.hhtank.beverage.controller;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hhtank.beverage.domain.Category;
import com.hhtank.beverage.exception.DataIntegrityException;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.service.CategoryService;
import com.hhtank.beverage.utils.RestResult;

/**
 * RESTful Controller for category management
 *
 * @author hhtank
 */
@RestController
public class RestCategoryController {

    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(RestCategoryController.class);
    @Autowired
    CategoryService                       categoryService;

    /**
     * Create a new category
     *
     * @param inputCat input category
     * @return creation result
     */
    @RequestMapping(value = "/rest/man/category", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<RestResult> create(@RequestBody final Category inputCat) {
        logger.info("input cat: ", inputCat);
        RestResult result = new RestResult();
        try {
            Category cat = categoryService.create(inputCat);
            result.setDataObject(cat);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (DataIntegrityViolationException e) {
            result.setErrorMessage("Category existed.");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            e.printStackTrace();
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Delete a category
     *
     * @param id category id
     * @return deleting result
     */
    @SuppressWarnings("boxing")
    @RequestMapping(value = "/rest/man/category/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ResponseEntity<RestResult> delete(@PathVariable final int id) {
        RestResult result = new RestResult();
        try {
            Category cat = new Category(id, "");
            categoryService.delete(cat);
            result.setDataObject(true);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (ItemNotFoundException e) {
            result.setErrorMessage("Requested item was not found");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (DataIntegrityException e) {
            result.setErrorMessage("Item can not be deleted");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Return list of all category
     *
     * @return list of category
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/rest/category", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity getAll() {
        RestResult result = new RestResult();
        try {
            List<Category> cats = categoryService.findAll();
            return new ResponseEntity(cats, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Get details of a category
     *
     * @param id category id
     * @return Category
     */
    @RequestMapping(value = "/rest/category/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<RestResult> getDetails(@PathVariable final int id) {
        RestResult result = new RestResult();
        try {
            Category cat = categoryService.findById(id);
            result.setDataObject(cat);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Update a category
     *
     * @param inputCat input category
     * @return merged result
     */
    @RequestMapping(value = "/rest/man/category", method = RequestMethod.PATCH, headers = "Accept=application/json")
    public ResponseEntity<RestResult> merge(@RequestBody final Category inputCat) {
        RestResult result = new RestResult();
        try {
            Category cat = categoryService.mergeOnly(inputCat);
            result.setDataObject(cat);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (ItemNotFoundException e) {
            result.setErrorMessage("Requested item was not found");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Update a category
     * 
     * @param inputCat input category
     * @return replaced result
     */
    @RequestMapping(value = "/rest/man/category", method = RequestMethod.PUT, headers = "Accept=application/json")
    public ResponseEntity<RestResult> replace(@RequestBody final Category inputCat) {
        RestResult result = new RestResult();
        try {
            Category cat = categoryService.update(inputCat);
            result.setDataObject(cat);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (ItemNotFoundException e) {
            result.setErrorMessage("Requested item was not found");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

}