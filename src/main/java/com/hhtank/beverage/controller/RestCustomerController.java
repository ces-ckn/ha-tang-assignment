package com.hhtank.beverage.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hhtank.beverage.domain.Beer;
import com.hhtank.beverage.domain.User;
import com.hhtank.beverage.domain.UserRole;
import com.hhtank.beverage.enums.EnumRole;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.service.BeerService;
import com.hhtank.beverage.service.UserService;
import com.hhtank.beverage.utils.CommonUtils;
import com.hhtank.beverage.utils.RestResult;

/**
 * RESTful controller used for customer
 *
 * @author hhtank
 */
@RestController
@RequestMapping("/rest/customer")
public class RestCustomerController {

    @Autowired
    BeerService                    beerService;
    @Autowired
    HttpSessionCsrfTokenRepository csrfRepo;
    @Autowired
    UserService                    userService;

    /**
     * Add a beer to a customer's passport
     *
     * @param id beer id to beer added
     * @param auth authentication info
     * @param request current request
     * @return creation result
     */
    @RequestMapping(value = "/beers/{id}", method = RequestMethod.POST)
    public ResponseEntity<RestResult> create(@PathVariable int id, @RequestParam String auth, HttpServletRequest request) {
        RestResult result = new RestResult();
        HttpHeaders headers = generateNewToken(request);
        try {
            // get authorization info
            String[] authArray = null;
            try {
                authArray = decodeAuthInfo(auth);
                if (authArray.length != 2) {
                    throw new Exception("Invalid authorization info");
                }
            }
            catch (Exception e1) {
                e1.printStackTrace();
                result.setErrorMessage("Invalid authorization info.");
                return new ResponseEntity<RestResult>(result, headers, HttpStatus.UNAUTHORIZED);
            }
            String username = authArray[0];
            String password = authArray[1];
            User u = verifyUser(username, password);
            if (u == null) {
                // unauthorized
                result.setErrorMessage("Access denied.");
                return new ResponseEntity<RestResult>(result, headers, HttpStatus.UNAUTHORIZED);
            }
            Beer b = new Beer();
            b.setId(id);
            u.getBeers().size();
            u.getBeers().add(b);
            userService.mergeOnly(u);

            result.setDataObject(u.getBeers());
            return new ResponseEntity<RestResult>(result, headers, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, headers, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    protected String[] decodeAuthInfo(String authInfo) {
        String auth = new String(Base64.decode(authInfo.getBytes()));
        String[] authArray = auth.split(":");
        return authArray;
    }

    protected HttpHeaders generateNewToken(HttpServletRequest request) {
        CsrfToken token = csrfRepo.generateToken(request);
        csrfRepo.saveToken(token, request, null);
        HttpHeaders headers = new HttpHeaders();
        headers.add(token.getHeaderName(), token.getToken());
        return headers;
    }

    /**
     * Return a list of all consumed beers of a customer and all other available
     * beers
     *
     * @param auth authentication info
     * @param request current request
     * @return list of beers
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/beers", method = RequestMethod.POST)
    public ResponseEntity getBeersListForCustomer(@RequestParam String auth, HttpServletRequest request) {
        RestResult result = new RestResult();
        HttpHeaders headers = generateNewToken(request);
        try {
            // get authorization info
            String[] authArray = null;
            try {
                authArray = decodeAuthInfo(auth);
                if (authArray.length != 2) {
                    throw new Exception("Invalid authorization info");
                }
            }
            catch (Exception e1) {
                e1.printStackTrace();
                result.setErrorMessage("Invalid authorization info.");
                return new ResponseEntity<RestResult>(result, headers, HttpStatus.UNAUTHORIZED);
            }
            String username = authArray[0];
            String password = authArray[1];
            User u = verifyUser(username, password);
            if (u == null) {
                // unauthorized
                result.setErrorMessage("Access denied.");
                return new ResponseEntity<RestResult>(result, headers, HttpStatus.UNAUTHORIZED);
            }
            List<Beer> beers = beerService.findBeerForCustomer(username);
            return new ResponseEntity(beers, headers, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, headers, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Return a token for accessing customer scope
     *
     * @param request current request
     * @return token
     */
    @RequestMapping(value = "/token")
    public ResponseEntity<String> getToken(HttpServletRequest request) {
        CsrfToken token = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
        HttpHeaders headers = new HttpHeaders();
        headers.add(token.getHeaderName(), token.getToken());
        return new ResponseEntity<>("Return access token", headers, HttpStatus.OK);
    }

    protected User verifyUser(String username, String password) {
        User u = null;
        try {
            u = userService.findByIdLazyload(username);
        }
        catch (ItemNotFoundException e) {
            return null;
        }
        if (!CommonUtils.checkPassword(password, u.getPassword())) {
            return null;
        }
        boolean hasCustomerRole = false;
        for (UserRole ur : u.getUserRoles()) {
            if (ur.getRoleName().equals(EnumRole.ROLE_CUTOMER.getValue())) {
                hasCustomerRole = true;
                break;
            }
        }
        if (!hasCustomerRole) {
            u = null;
        }
        return u;
    }

}