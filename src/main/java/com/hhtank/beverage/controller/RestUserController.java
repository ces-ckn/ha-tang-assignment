package com.hhtank.beverage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hhtank.beverage.domain.User;
import com.hhtank.beverage.domain.UserRole;
import com.hhtank.beverage.enums.EnumRole;
import com.hhtank.beverage.enums.EnumUserStatus;
import com.hhtank.beverage.exception.DuplicatedObjectException;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.service.UserService;
import com.hhtank.beverage.utils.CommonUtils;
import com.hhtank.beverage.utils.RestResult;

/**
 * RESTful controller for User management
 *
 * @author hhtank
 */
@RestController
public class RestUserController {

    @Autowired
    UserService userService;

    /**
     * Change password for a user
     *
     * @param username current user
     * @param currentpass current password
     * @param changedpass new password
     * @return changing result
     */
    @SuppressWarnings("boxing")
    @RequestMapping(value = "/rest/man/user/{username}/password", method = RequestMethod.PUT, headers = "Accept=application/json")
    public ResponseEntity<RestResult> changePassword(@PathVariable String username, @RequestParam String currentpass,
                                                     @RequestParam String changedpass) {
        RestResult result = new RestResult();
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if ((auth.getName() == null) || !auth.getName().equals(username)) {
                // not current user
                result.setErrorMessage("Access denied.");
                return new ResponseEntity<RestResult>(result, HttpStatus.UNAUTHORIZED);
            }
            User updating = userService.findById(username);
            if ((updating == null) || !CommonUtils.checkPassword(currentpass, updating.getPassword())) {
                // not current user
                result.setErrorMessage("Access denied.");
                return new ResponseEntity<RestResult>(result, HttpStatus.UNAUTHORIZED);
            }
            User user = new User(username, CommonUtils.hashingPassword(changedpass));
            userService.mergeOnly(user);
            result.setDataObject(true);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Create a new admin user
     *
     * @param inputUser input user
     * @return creation result
     */
    @RequestMapping(value = "/rest/man/user", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<RestResult> create(@RequestBody final User inputUser) {
        RestResult result = new RestResult();
        try {
            UserRole userRole = new UserRole(inputUser, EnumRole.ROLE_ADMIN.getValue());
            inputUser.getUserRoles().add(userRole);
            inputUser.setPassword(CommonUtils.hashingPassword(inputUser.getPassword()));
            User user = userService.create(inputUser);
            result.setDataObject(user);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (DuplicatedObjectException e) {
            result.setErrorMessage("Duplicated username.");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Delete a user
     *
     * @param username username
     * @return deleting result
     */
    @SuppressWarnings("boxing")
    @RequestMapping(value = "/rest/man/user/{username}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ResponseEntity<RestResult> delete(@PathVariable final String username) {
        RestResult result = new RestResult();
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (username.equals(auth.getName())) {
                // can not delete current user
                result.setErrorMessage("Action not allowed.");
                return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
            }
            userService.delete(new User(username, ""));
            result.setDataObject(true);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (ItemNotFoundException e) {
            result.setErrorMessage("Requested item was not found");
            return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Return all administrative users
     *
     * @return users
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/rest/man/user", method = RequestMethod.GET)
    public ResponseEntity getAllAdmin() {
        RestResult result = new RestResult();
        try {
            List<User> users = userService.findAllAdmin();
            return new ResponseEntity(users, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Update a user
     *
     * @param inputUser input user
     * @return updating result
     */
    @RequestMapping(value = "/rest/man/user", method = RequestMethod.PATCH, headers = "Accept=application/json")
    public ResponseEntity<RestResult> merge(@RequestBody final User inputUser) {
        RestResult result = new RestResult();
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (inputUser.getUsername().equals(auth.getName()) && (inputUser.getEnabled() == EnumUserStatus.DISABLED.getValue())) {
                // can not disable current user
                result.setErrorMessage("Action not allowed.");
                return new ResponseEntity<RestResult>(result, HttpStatus.BAD_REQUEST);
            }
            User user = userService.mergeOnly(inputUser);
            result.setDataObject(user);
            return new ResponseEntity<RestResult>(result, HttpStatus.OK);
        }
        catch (Exception e) {
            result.setErrorMessage("Server error: " + e.getMessage());
            return new ResponseEntity<RestResult>(result, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

}