package com.hhtank.beverage.enums;

/**
 * Enum used for User's status
 *
 * @author hhtank
 */
public enum EnumUserStatus {
    /**
     * Disabled
     */
    DISABLED(2),
    /**
     * Enabled
     */
    ENABLED(1);

    private int value;

    private EnumUserStatus(int value) {
        this.value = value;
    }

    @SuppressWarnings("javadoc")
    public int getValue() {
        return value;
    }
}
