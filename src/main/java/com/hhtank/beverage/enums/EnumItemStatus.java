package com.hhtank.beverage.enums;

/**
 * Enum used for status of an item (beer)
 *
 * @author hhtank
 */
public enum EnumItemStatus {
    /**
     * Archived
     */
    ARCHIVED(2),
    /**
     * Available
     */
    AVAILABLE(1);

    private int value;

    private EnumItemStatus(int value) {
        this.value = value;
    }

    @SuppressWarnings("javadoc")
    public int getValue() {
        return value;
    }
}
