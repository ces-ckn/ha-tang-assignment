package com.hhtank.beverage.enums;

/**
 * Enum used for UserRole
 *
 * @author hhtank
 */
public enum EnumRole {
    /**
     * Role Admin
     */
    ROLE_ADMIN("ROLE_ADMIN"),
    /**
     * Role customer
     */
    ROLE_CUTOMER("ROLE_CUTOMER"),
    /**
     * Role user
     */
    ROLE_USER("ROLE_USER");

    private String value;

    private EnumRole(String value) {
        this.value = value;
    }

    @SuppressWarnings("javadoc")
    public String getValue() {
        return value;
    }
}
