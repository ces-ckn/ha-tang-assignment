package com.hhtank.beverage.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The UserRole entity.
 */
@Entity
@Table(name = "user_roles")
public class UserRole {

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_role_id")
    private int    id;

    /** The role name. */
    @Column(name = "role_name", nullable = false)
    private String roleName;

    /** The user. */
    @ManyToOne(optional = false)
    @JoinColumn(name = "username")
    private User   user;

    /**
     * Instantiates a new user role.
     */
    public UserRole() {
    }

    /**
     * Instantiates a new user role.
     *
     * @param user the user
     * @param roleName the role name
     */
    public UserRole(User user, String roleName) {
        this.user = user;
        this.roleName = roleName;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the role name.
     *
     * @return the role name
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the role name.
     *
     * @param roleName the new role name
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(User user) {
        this.user = user;
    }
}
