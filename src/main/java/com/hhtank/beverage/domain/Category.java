package com.hhtank.beverage.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The Category entity.
 */
@Entity
@Table(name = "beer_category")
public class Category {

    /** The beers. */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category", fetch = FetchType.LAZY)
    private Set<Beer> beers = new HashSet<Beer>();

    /** The cat name. */
    @Column(name = "cat_name", unique = true, nullable = false)
    private String    catName;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cat_id")
    private int       id;

    /**
     * Instantiates a new category.
     */
    public Category() {
    }

    /**
     * Instantiates a new category.
     *
     * @param id the id
     * @param catName the cat name
     */
    public Category(int id, String catName) {
        this.id = id;
        this.catName = catName;
    }

    /**
     * Instantiates a new category.
     *
     * @param catName the cat name
     */
    public Category(String catName) {
        this.catName = catName;
    }

    /**
     * Gets the beers.
     *
     * @return the beers
     */
    @JsonIgnore
    public Set<Beer> getBeers() {
        return beers;
    }

    /**
     * Gets the cat name.
     *
     * @return the cat name
     */
    public String getCatName() {
        return catName;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the beers.
     *
     * @param beers the new beers
     */
    public void setBeers(Set<Beer> beers) {
        if (beers != null) {
            this.beers = beers;
        }
    }

    /**
     * Sets the cat name.
     *
     * @param catName the new cat name
     */
    public void setCatName(String catName) {
        this.catName = catName;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(int id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Task [id=" + id + ", catName=" + catName + "]";
    }
}
