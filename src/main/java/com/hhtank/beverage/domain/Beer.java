package com.hhtank.beverage.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hhtank.beverage.enums.EnumItemStatus;

/**
 * The Beer entity.
 */
@Entity
@Table(name = "beer")
public class Beer {

    /** The beer name. */
    @Column(name = "beer_name", nullable = false, unique = true)
    private String    beerName;

    /** The category. */
    @ManyToOne(optional = false)
    @JoinColumn(name = "cat_id")
    private Category  category;

    /** The country. */
    @Column(name = "country")
    private String    country;

    /** The description. */
    @Column(name = "description")
    private String    description;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "beer_id")
    private int       id;

    /** The manufactor. */
    @Column(name = "manufactor")
    private String    manufactor;

    /** The price. */
    @Column(name = "price")
    private float     price;

    /** The status. */
    @Column(name = "status")
    private int       status;

    /** The users. */
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "beers")
    private Set<User> users = new HashSet<User>();

    /**
     * Instantiates a new beer.
     */
    public Beer() {
        status = EnumItemStatus.AVAILABLE.getValue();
    }

    /**
     * Instantiates a new beer.
     *
     * @param name the name
     * @param category the category
     */
    public Beer(String name, Category category) {
        status = EnumItemStatus.AVAILABLE.getValue();
        beerName = name;
        this.category = category;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @SuppressWarnings("boxing")
    @Override
    public boolean equals(Object obj) {
        return equals(((Beer) obj).getId());
    }

    /**
     * Gets the beer name.
     *
     * @return the beer name
     */
    public String getBeerName() {
        return beerName;
    }

    /**
     * Gets the category.
     *
     * @return the category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Gets the country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the manufactor.
     *
     * @return the manufactor
     */
    public String getManufactor() {
        return manufactor;
    }

    /**
     * Gets the price.
     *
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * Gets the users.
     *
     * @return the users
     */
    public Set<User> getUsers() {
        return users;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Sets the beer name.
     *
     * @param beerName the new beer name
     */
    public void setBeerName(String beerName) {
        this.beerName = beerName;
    }

    /**
     * Sets the category.
     *
     * @param category the new category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * Sets the country.
     *
     * @param country the new country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the manufactor.
     *
     * @param manufactor the new manufactor
     */
    public void setManufactor(String manufactor) {
        this.manufactor = manufactor;
    }

    /**
     * Sets the price.
     *
     * @param price the new price
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * Sets the status.
     *
     * @param status the new status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Sets the users.
     *
     * @param users the new users
     */
    public void setUsers(Set<User> users) {
        this.users = users;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Task [id=" + id + ", beerName=" + beerName + "]";
    }
}
