package com.hhtank.beverage.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hhtank.beverage.enums.EnumUserStatus;

/**
 * The User entity.
 */
@Entity
@Table(name = "users")
public class User {

    /** The beers. */
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "CUSTOMER_AND_BEERS")
    private Set<Beer>     beers     = new HashSet<Beer>();

    /** The enabled. */
    @Column(name = "enabled")
    private int           enabled;

    /** The password. */
    @Column(name = "password", nullable = false)
    private String        password;

    /** The username. */
    @Id
    @Column(name = "username")
    private String        username;

    /** The user roles. */
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private Set<UserRole> userRoles = new HashSet<UserRole>();

    /**
     * Instantiates a new user.
     */
    public User() {
    }

    /**
     * Instantiates a new user.
     *
     * @param username the username
     * @param enabled the enabled
     */
    public User(String username, int enabled) {
        this.username = username;
        password = "";
        this.enabled = enabled;
    }

    /**
     * Instantiates a new user.
     *
     * @param username the username
     * @param password the password
     */
    public User(String username, String password) {
        this.username = username;
        this.password = password;
        enabled = EnumUserStatus.ENABLED.getValue();
    }

    /**
     * Gets the beers.
     *
     * @return the beers
     */
    public Set<Beer> getBeers() {
        return beers;
    }

    /**
     * Gets the enabled.
     *
     * @return the enabled
     */
    public int getEnabled() {
        return enabled;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets the username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets the user roles.
     *
     * @return the user roles
     */
    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    /**
     * Sets the beers.
     *
     * @param beers the new beers
     */
    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }

    /**
     * Sets the enabled.
     *
     * @param enabled the new enabled
     */
    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets the username.
     *
     * @param username the new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Sets the user roles.
     *
     * @param userRoles the new user roles
     */
    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }
}
