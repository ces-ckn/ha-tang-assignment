package com.hhtank.beverage.exception;

/**
 * Item not found exception
 * 
 * @author hhtank
 */
public class ItemNotFoundException
    extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
