package com.hhtank.beverage.exception;

/**
 * @author hhtank
 */
public class DuplicatedObjectException
extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3839142528434522269L;

}
