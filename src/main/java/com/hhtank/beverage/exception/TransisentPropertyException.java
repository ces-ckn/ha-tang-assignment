package com.hhtank.beverage.exception;

/**
 * Transisent property exception
 * 
 * @author hhtank
 */
public class TransisentPropertyException
    extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -1899452859154708127L;

}
