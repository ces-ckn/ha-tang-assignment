package com.hhtank.beverage.exception;

/**
 * Data integrity exception
 * 
 * @author hhtank
 */
public class DataIntegrityException
    extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -121779712590212795L;

}
