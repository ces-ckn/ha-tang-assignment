package com.hhtank.beverage.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hhtank.beverage.domain.User;

/**
 * @author hhtank
 */
@Repository
public interface UserRepository
    extends CrudRepository<User, String> {

    /**
     * Return list of users without password
     * 
     * @return List<User>
     */
    @Query("select new User(u.username, u.enabled) from User u")
    List<User> findAllRestrictPassword();

    /**
     * Find by role, without password
     * 
     * @param roleName
     * @return List<User>
     */
    @Query("select new User(u.username, u.enabled) from User u join u.userRoles ur where ur.roleName=?1")
    List<User> findByRoleNameRestrictPassword(String roleName);
}
