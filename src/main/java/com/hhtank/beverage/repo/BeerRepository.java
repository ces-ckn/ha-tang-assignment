package com.hhtank.beverage.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hhtank.beverage.domain.Beer;

/**
 * Beer repository
 * 
 * @author hhtank
 */
@Repository
public interface BeerRepository
    extends CrudRepository<Beer, Integer> {

    /**
     * Find list of beer for customer scope
     * 
     * @param username
     * @return List<Beer>
     */
    @Query("SELECT b FROM Beer b left join b.users users where users.username = ?1 or b.status=1")
    List<Beer> findBeerForCustomer(String username);

    /**
     * Find by status
     * 
     * @param value
     * @return List<Beer>
     */
    @Query("SELECT b FROM Beer b where b.status = ?1")
    List<Beer> findByStatus(int value);

}
