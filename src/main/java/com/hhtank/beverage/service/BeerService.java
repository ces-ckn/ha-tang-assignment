package com.hhtank.beverage.service;

import java.util.List;

import com.hhtank.beverage.domain.Beer;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.exception.TransisentPropertyException;

/**
 * Interface to handle business logic of beer management.
 *
 * @author hhtank
 */
public interface BeerService {

    /**
     * Archive a beer
     *
     * @param id the id
     * @throws ItemNotFoundException the item not found exception
     */
    public void archive(int id) throws ItemNotFoundException;

    /**
     * Creates a beer
     *
     * @param beer the beer
     * @return the beer
     * @throws TransisentPropertyException the transisent property exception
     */
    public Beer create(Beer beer) throws TransisentPropertyException;

    /**
     * Delete a beer
     *
     * @param beer the beer
     * @throws ItemNotFoundException the item not found exception
     */
    public void delete(Beer beer) throws ItemNotFoundException;

    /**
     * Find all for management scope
     *
     * @return List<Beer>
     */
    public List<Beer> findAll();

    /**
     * Find beer for customer.
     *
     * @param username the username
     * @return the list
     */
    List<Beer> findBeerForCustomer(String username);

    /**
     * Find by id.
     *
     * @param id the id
     * @return the beer
     */
    public Beer findById(int id);

    /**
     * Find all available for public scope.
     *
     * @return the list
     */
    public List<Beer> findPublic();

    /**
     * Find details for public scope.
     *
     * @param id the id
     * @return the beer
     */
    public Beer findPublic(int id);

    /**
     * Merge only.
     *
     * @param patching the patching
     * @return the beer
     * @throws ItemNotFoundException the item not found exception
     * @throws TransisentPropertyException the transisent property exception
     */
    public Beer mergeOnly(Beer patching) throws ItemNotFoundException,
                                        TransisentPropertyException;

    /**
     * Update.
     *
     * @param beer the beer
     * @return the beer
     * @throws ItemNotFoundException the item not found exception
     * @throws TransisentPropertyException the transisent property exception
     */
    public Beer update(Beer beer) throws ItemNotFoundException,
                                 TransisentPropertyException;

}
