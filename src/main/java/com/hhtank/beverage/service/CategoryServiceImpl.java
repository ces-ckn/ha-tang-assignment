package com.hhtank.beverage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hhtank.beverage.domain.Category;
import com.hhtank.beverage.exception.DataIntegrityException;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.repo.CategoryRepository;

/**
 * The implementation of CategoryService.
 *
 * @author hhtank
 */
@Component
public class CategoryServiceImpl
    implements CategoryService {

    /** The repository. */
    @Autowired
    CategoryRepository repository;

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.CategoryService#create(com.hhtank.beverage
     * .domain.Category)
     */
    @Override
    @Transactional
    public Category create(Category category) {
        return repository.save(category);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.CategoryService#delete(com.hhtank.beverage
     * .domain.Category)
     */
    @SuppressWarnings("boxing")
    @Override
    @Transactional(rollbackFor = { ItemNotFoundException.class, DataIntegrityException.class })
    public void delete(Category category) throws ItemNotFoundException,
                                         DataIntegrityException {
        Category deleted = repository.findOne(category.getId());

        if (deleted == null) {
            throw new ItemNotFoundException();
        }
        if ((deleted.getBeers() != null) && (deleted.getBeers().size() > 0)) {
            throw new DataIntegrityException();
        }
        repository.delete(deleted);
    }

    /*
     * (non-Javadoc)
     * @see com.hhtank.beverage.service.CategoryService#findAll()
     */
    @Override
    public List<Category> findAll() {
        return (List<Category>) repository.findAll();
    }

    /*
     * (non-Javadoc)
     * @see com.hhtank.beverage.service.CategoryService#findById(int)
     */
    @SuppressWarnings("boxing")
    @Override
    public Category findById(int id) {
        return repository.findOne(id);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.CategoryService#mergeOnly(com.hhtank.beverage
     * .domain.Category)
     */
    @SuppressWarnings("boxing")
    @Override
    @Transactional(rollbackFor = ItemNotFoundException.class)
    public Category mergeOnly(Category category) throws ItemNotFoundException {
        Category updated = repository.findOne(category.getId());

        if (updated == null) {
            throw new ItemNotFoundException();
        }

        if ((category.getCatName() != null) && !category.getCatName().isEmpty()) {
            updated.setCatName(category.getCatName());
        }
        if ((category.getBeers() != null) && !category.getBeers().isEmpty()) {
            updated.setBeers(category.getBeers());
        }
        return repository.save(updated);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.CategoryService#update(com.hhtank.beverage
     * .domain.Category)
     */
    @SuppressWarnings("boxing")
    @Override
    @Transactional(rollbackFor = ItemNotFoundException.class)
    public Category update(Category category) throws ItemNotFoundException {
        Category updated = repository.findOne(category.getId());

        if (updated == null) {
            throw new ItemNotFoundException();
        }

        updated.setCatName(category.getCatName());
        updated.setBeers(category.getBeers());
        return repository.save(updated);
    }

}
