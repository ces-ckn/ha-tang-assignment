package com.hhtank.beverage.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hhtank.beverage.domain.Beer;
import com.hhtank.beverage.enums.EnumItemStatus;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.exception.TransisentPropertyException;
import com.hhtank.beverage.repo.BeerRepository;

/**
 * Implementation of BeerService
 *
 * @author hhtank
 */
@Component
public class BeerServiceImpl
    implements BeerService {

    @Autowired
    BeerRepository repository;

    @Override
    @Transactional(rollbackFor = ItemNotFoundException.class)
    public void archive(int id) throws ItemNotFoundException {
        @SuppressWarnings("boxing")
        Beer updated = repository.findOne(id);

        if (updated == null) {
            throw new ItemNotFoundException();
        }

        updated.setStatus(EnumItemStatus.ARCHIVED.getValue());
        repository.save(updated);
    }

    @SuppressWarnings("unused")
    @Override
    @Transactional(rollbackFor = TransisentPropertyException.class)
    public Beer create(Beer beer) throws TransisentPropertyException {
        return repository.save(beer);
    }

    @Override
    @Transactional(rollbackFor = ItemNotFoundException.class)
    public void delete(Beer beer) throws ItemNotFoundException {
        @SuppressWarnings("boxing")
        Beer deleted = repository.findOne(beer.getId());

        if (deleted == null) {
            throw new ItemNotFoundException();
        }
        repository.delete(deleted);
    }

    @Override
    public List<Beer> findAll() {
        return (List<Beer>) repository.findAll();
    }

    @Override
    public List<Beer> findBeerForCustomer(String username) {
        return repository.findBeerForCustomer(username);
    }

    @SuppressWarnings("boxing")
    @Override
    public Beer findById(int id) {
        return repository.findOne(id);
    }

    @Override
    public List<Beer> findPublic() {
        return repository.findByStatus(EnumItemStatus.AVAILABLE.getValue());
    }

    @SuppressWarnings("boxing")
    @Override
    public Beer findPublic(int id) {
        Beer beer = repository.findOne(id);
        if ((beer != null) && (beer.getStatus() == EnumItemStatus.AVAILABLE.getValue())) {
            return beer;
        }
        return null;
    }

    @SuppressWarnings({ "unused", "boxing" })
    @Override
    @Transactional(rollbackFor = ItemNotFoundException.class)
    public Beer mergeOnly(Beer beer) throws ItemNotFoundException,
                                    TransisentPropertyException {
        Beer updated = repository.findOne(beer.getId());

        if (updated == null) {
            throw new ItemNotFoundException();
        }

        if (!StringUtils.isEmpty(beer.getBeerName())) {
            updated.setBeerName(beer.getBeerName());
        }
        if ((beer.getCategory() != null) && (beer.getCategory().getId() > 0)) {
            updated.setCategory(beer.getCategory());
        }
        if (!StringUtils.isEmpty(beer.getCountry())) {
            updated.setCountry(beer.getCountry());
        }
        if (!StringUtils.isEmpty(beer.getDescription())) {
            updated.setDescription(beer.getDescription());
        }
        if (!StringUtils.isEmpty(beer.getManufactor())) {
            updated.setManufactor(beer.getManufactor());
        }
        if (beer.getPrice() > 0) {
            updated.setPrice(beer.getPrice());
        }
        if ((beer.getStatus() == EnumItemStatus.AVAILABLE.getValue()) || (beer.getStatus() == EnumItemStatus.ARCHIVED.getValue())) {
            updated.setStatus(beer.getStatus());
        }
        return repository.save(updated);
    }

    @SuppressWarnings({ "unused", "boxing" })
    @Override
    @Transactional(rollbackFor = ItemNotFoundException.class)
    public Beer update(Beer beer) throws ItemNotFoundException,
                                 TransisentPropertyException {
        Beer updated = repository.findOne(beer.getId());

        if (updated == null) {
            throw new ItemNotFoundException();
        }

        updated.setBeerName(beer.getBeerName());
        updated.setCategory(beer.getCategory());
        updated.setCountry(beer.getCountry());
        updated.setDescription(beer.getDescription());
        updated.setManufactor(beer.getManufactor());
        updated.setPrice(beer.getPrice());
        updated.setStatus(beer.getStatus());
        return repository.save(updated);
    }
}
