package com.hhtank.beverage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.hhtank.beverage.domain.User;
import com.hhtank.beverage.enums.EnumRole;
import com.hhtank.beverage.enums.EnumUserStatus;
import com.hhtank.beverage.exception.DataIntegrityException;
import com.hhtank.beverage.exception.DuplicatedObjectException;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.repo.UserRepository;

/**
 * The Implementation of UserService.
 */
@Component
public class UserServiceImpl
    implements UserService {

    /** The repository. */
    @Autowired
    UserRepository repository;

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.UserService#create(com.hhtank.beverage.domain
     * .User)
     */
    @Override
    @Transactional
    public User create(User user) throws DuplicatedObjectException {
        if (repository.exists(user.getUsername())) {
            throw new DuplicatedObjectException();
        }
        return repository.save(user);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.UserService#delete(com.hhtank.beverage.domain
     * .User)
     */
    @Override
    @Transactional(rollbackFor = { ItemNotFoundException.class, DataIntegrityException.class })
    public void delete(User user) throws ItemNotFoundException {
        User deleted = repository.findOne(user.getUsername());

        if (deleted == null) {
            throw new ItemNotFoundException();
        }
        repository.delete(deleted);
    }

    /*
     * (non-Javadoc)
     * @see com.hhtank.beverage.service.UserService#findAllAdmin()
     */
    @Override
    public List<User> findAllAdmin() {
        return repository.findByRoleNameRestrictPassword(EnumRole.ROLE_ADMIN.getValue());
    }

    /*
     * (non-Javadoc)
     * @see com.hhtank.beverage.service.UserService#findById(java.lang.String)
     */
    @Override
    public User findById(String username) {
        return repository.findOne(username);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.UserService#findByIdLazyload(java.lang.String
     * )
     */
    @Override
    @Transactional
    public User findByIdLazyload(String username) throws ItemNotFoundException {
        User u = repository.findOne(username);
        if (u == null) {
            throw new ItemNotFoundException();
        }
        u.getBeers().size();
        u.getUserRoles().size();
        return u;
    }

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.UserService#mergeOnly(com.hhtank.beverage
     * .domain.User)
     */
    @Override
    @Transactional(rollbackFor = ItemNotFoundException.class)
    public User mergeOnly(User user) throws ItemNotFoundException {
        User updated = repository.findOne(user.getUsername());

        if (updated == null) {
            throw new ItemNotFoundException();
        }

        if ((user.getPassword() != null) && !user.getPassword().isEmpty()) {
            updated.setPassword(user.getPassword());
        }

        if ((user.getEnabled() == EnumUserStatus.ENABLED.getValue()) || (user.getEnabled() == EnumUserStatus.DISABLED.getValue())) {
            updated.setEnabled(user.getEnabled());
        }
        if (user.getBeers() != null) {
            updated.setBeers(user.getBeers());
        }
        return repository.save(updated);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.hhtank.beverage.service.UserService#update(com.hhtank.beverage.domain
     * .User)
     */
    @Override
    @Transactional(rollbackFor = ItemNotFoundException.class)
    public User update(User user) throws ItemNotFoundException {
        User updated = repository.findOne(user.getUsername());

        if (updated == null) {
            throw new ItemNotFoundException();
        }

        updated.setPassword(user.getPassword());
        updated.setEnabled(user.getEnabled());
        updated.setBeers(user.getBeers());
        updated.setUserRoles(user.getUserRoles());
        return repository.save(updated);
    }

}
