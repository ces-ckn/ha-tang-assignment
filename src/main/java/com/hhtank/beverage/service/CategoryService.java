package com.hhtank.beverage.service;

import java.util.List;

import com.hhtank.beverage.domain.Category;
import com.hhtank.beverage.exception.DataIntegrityException;
import com.hhtank.beverage.exception.ItemNotFoundException;

/**
 * Interface to handle business logic of category management.
 *
 * @author hhtank
 */
public interface CategoryService {

    /**
     * Creates a category
     *
     * @param category the category
     * @return the category
     */
    public Category create(Category category);

    /**
     * Delete a category
     *
     * @param category the category
     * @throws ItemNotFoundException the item not found exception
     * @throws DataIntegrityException the data integrity exception
     */
    public void delete(Category category) throws ItemNotFoundException,
                                         DataIntegrityException;

    /**
     * Find all.
     *
     * @return the list
     */
    public List<Category> findAll();

    /**
     * Find by id.
     *
     * @param id the id
     * @return the category
     */
    public Category findById(int id);

    /**
     * Merge only.
     *
     * @param inputCat the input cat
     * @return the category
     * @throws ItemNotFoundException the item not found exception
     */
    public Category mergeOnly(Category inputCat) throws ItemNotFoundException;

    /**
     * Update.
     *
     * @param category the category
     * @return the category
     * @throws ItemNotFoundException the item not found exception
     */
    public Category update(Category category) throws ItemNotFoundException;

}
