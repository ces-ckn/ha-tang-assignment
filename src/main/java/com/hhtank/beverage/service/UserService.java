package com.hhtank.beverage.service;

import java.util.List;

import com.hhtank.beverage.domain.User;
import com.hhtank.beverage.exception.DuplicatedObjectException;
import com.hhtank.beverage.exception.ItemNotFoundException;

/**
 * The Interface to hanlde business logic for user management.
 */
public interface UserService {

    /**
     * Creates a user
     *
     * @param user the user
     * @return the user
     * @throws DuplicatedObjectException the duplicated object exception
     */
    public User create(User user) throws DuplicatedObjectException;

    /**
     * Delete a user
     *
     * @param user the user
     * @throws ItemNotFoundException the item not found exception
     */
    public void delete(User user) throws ItemNotFoundException;

    /**
     * Find all admin users
     *
     * @return the list
     */
    public List<User> findAllAdmin();

    /**
     * Find by id.
     *
     * @param username the username
     * @return the user
     */
    public User findById(String username);

    /**
     * Find by id with lazyload.
     *
     * @param username the username
     * @return the user
     * @throws ItemNotFoundException the item not found exception
     */
    User findByIdLazyload(String username) throws ItemNotFoundException;

    /**
     * Merge only.
     *
     * @param user the user
     * @return the user
     * @throws ItemNotFoundException the item not found exception
     */
    User mergeOnly(User user) throws ItemNotFoundException;

    /**
     * Update.
     *
     * @param user the user
     * @return the user
     * @throws ItemNotFoundException the item not found exception
     */
    public User update(User user) throws ItemNotFoundException;
}
