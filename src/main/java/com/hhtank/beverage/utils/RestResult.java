package com.hhtank.beverage.utils;

/**
 * @author hhtank
 */
public class RestResult {

    private Object dataObject;
    private String errorMessage;

    @SuppressWarnings("javadoc")
    public Object getDataObject() {
        return dataObject;
    }

    @SuppressWarnings("javadoc")
    public String getErrorMessage() {
        return errorMessage;
    }

    @SuppressWarnings("javadoc")
    public void setDataObject(Object dataObject) {
        this.dataObject = dataObject;
    }

    @SuppressWarnings("javadoc")
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
