package com.hhtank.beverage.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * The Class CommonUtils.
 */
public class CommonUtils {

    /** The Constant encoder. */
    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    /**
     * Check password.
     *
     * @param pass the pass
     * @param hashedPass the hashed pass
     * @return true, if successful
     */
    public static boolean checkPassword(String pass, String hashedPass) {
        return encoder.matches(pass, hashedPass);
    }

    /**
     * Hashing password.
     *
     * @param pass the pass
     * @return the string
     */
    public static String hashingPassword(String pass) {
        return encoder.encode(pass);
    }
}
