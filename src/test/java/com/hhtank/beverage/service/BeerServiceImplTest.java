/**
 *
 */
package com.hhtank.beverage.service;

import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.hhtank.beverage.domain.Beer;
import com.hhtank.beverage.domain.Category;
import com.hhtank.beverage.domain.User;
import com.hhtank.beverage.enums.EnumItemStatus;
import com.hhtank.beverage.exception.DuplicatedObjectException;
import com.hhtank.beverage.exception.ItemNotFoundException;
import com.hhtank.beverage.exception.TransisentPropertyException;

/**
 * The test class for BeerService.
 *
 * @author hhtank
 */
@RunWith(org.springframework.test.context.junit4.SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:beans-test.xml")
@Transactional
public class BeerServiceImplTest
extends TestCase {

    /** The beer service. */
    @Autowired
    BeerService     beerService;

    /** The category service. */
    @Autowired
    CategoryService categoryService;

    @Autowired
    UserService     userService;

    /**
     * Before test.
     *
     * @throws Exception the exception
     */
    @Before
    public void beforeTest() throws Exception {
        Category cat = new Category("cat-test-00");
        cat = categoryService.create(cat);
        Beer beer = new Beer();
        beer.setBeerName("beer-test-00");
        beer.setCategory(cat);
        beer = beerService.create(beer);
    }

    /*
     * (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#archive(int)} .
     *
     * @throws ItemNotFoundException the item not found exception
     */
    @Test
    public void testArchive() throws ItemNotFoundException {
        Beer beer = beerService.findAll().get(0);
        int id = beer.getId();
        beerService.archive(id);
        Beer found = beerService.findById(id);
        assertEquals(EnumItemStatus.ARCHIVED.getValue(), found.getStatus());

    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#archive(int)} .
     */
    @Test
    public void testArchiveNotFound() {
        try {
            beerService.archive(-100);
        }
        catch (Exception e) {
            assertSame(ItemNotFoundException.class, e.getClass());
        }
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#create(com.hhtank.beverage.domain.Beer)}
     * .
     *
     * @throws TransisentPropertyException the transisent property exception
     */
    @Test
    public void testCreate() throws TransisentPropertyException {
        Category cat = categoryService.findAll().get(0);
        Beer beer = new Beer();
        beer.setBeerName("beer-test-01");
        beer.setCategory(cat);
        beer = beerService.create(beer);
        assertEquals(true, beer.getId() > 0);
        assertEquals(EnumItemStatus.AVAILABLE.getValue(), beer.getStatus());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#delete(com.hhtank.beverage.domain.Beer)}
     * .
     *
     * @throws ItemNotFoundException the item not found exception
     */
    @Test
    public void testDelete() throws ItemNotFoundException {
        int size = beerService.findAll().size();
        Beer beer = beerService.findAll().get(0);
        beerService.delete(beer);
        assertEquals(size - 1, beerService.findAll().size());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#delete(com.hhtank.beverage.domain.Beer)}
     * .
     */
    @Test
    public void testDeleteNotFound() {
        Beer beer = new Beer();
        beer.setId(-100);
        beer.setBeerName("beer-test-01");
        try {
            beerService.delete(beer);
        }
        catch (Exception e) {
            assertSame(ItemNotFoundException.class, e.getClass());
        }
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#findAll()}.
     */
    @Test
    public void testFindAll() {
        assertEquals(true, beerService.findAll().size() > 0);
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#findBeerForCustomer(String)}
     * .
     *
     * @throws TransisentPropertyException the transisent property exception
     * @throws DuplicatedObjectException duplicated object exception
     * @throws ItemNotFoundException item not found
     */
    @Test
    public void testFindBeersForCustomer() throws TransisentPropertyException,
    DuplicatedObjectException,
    ItemNotFoundException {
        Category cat = categoryService.findAll().get(0);
        Beer beer = new Beer("beer-test-01", cat);
        beer.setStatus(EnumItemStatus.ARCHIVED.getValue());
        beer = beerService.create(beer);
        assertEquals(true, beer.getId() > 0);
        User user = new User("user-test-01", "123");
        user = userService.create(user);
        user.getBeers().add(beer);
        userService.mergeOnly(user);
        List<Beer> beers = beerService.findBeerForCustomer("user-test-01");
        Beer foundBeer = new Beer();
        for (Beer b : beers) {
            if (b.getBeerName().equals("beer-test-01")) {
                foundBeer = b;
                break;
            }
        }
        assertTrue(foundBeer.getId() > 0);
        assertEquals(EnumItemStatus.ARCHIVED.getValue(), foundBeer.getStatus());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#findById(int)}.
     */
    @Test
    public void testFindById() {
        int id = beerService.findAll().get(0).getId();
        assertNotNull(beerService.findById(id));
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#findPublic()}.
     */
    @Test
    public void testFindPublic() {
        assertEquals(true, beerService.findPublic().size() > 0);
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#findPublic()}.
     */
    @Test
    public void testFindPublicWithId() {
        Beer beer = beerService.findPublic().get(0);
        int id = beer.getId();
        assertEquals(EnumItemStatus.AVAILABLE.getValue(), beerService.findPublic(id).getStatus());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#update(com.hhtank.beverage.domain.Beer)}
     * .
     *
     * @throws ItemNotFoundException the item not found exception
     * @throws TransisentPropertyException the transisent property exception
     */
    @Test
    public void testUpdate() throws ItemNotFoundException,
    TransisentPropertyException {
        Beer beer = beerService.findAll().get(0);
        int id = beer.getId();
        beer.setBeerName("beer-test-01-changed");
        beerService.update(beer);
        Beer found = beerService.findById(id);
        assertEquals("beer-test-01-changed", found.getBeerName());

    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.BeerServiceImpl#update(com.hhtank.beverage.domain.Beer)}
     * .
     */
    @Test
    public void testUpdateNotFound() {
        Beer beer = new Beer();
        beer.setId(-100);
        beer.setBeerName("beer-test-01-changed");
        try {
            beerService.update(beer);
        }
        catch (Exception e) {
            assertSame(ItemNotFoundException.class, e.getClass());
        }
    }
}
