/**
 *
 */
package com.hhtank.beverage.service;

import java.util.HashSet;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.hhtank.beverage.domain.Beer;
import com.hhtank.beverage.domain.Category;
import com.hhtank.beverage.exception.DataIntegrityException;
import com.hhtank.beverage.exception.ItemNotFoundException;

/**
 * The test class for CategoryService.
 *
 * @author hhtank
 */
@RunWith(org.springframework.test.context.junit4.SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:beans-test.xml")
@Transactional
public class CategoryServiceImplTest
extends TestCase {

    /** The category service. */
    @Autowired
    CategoryService categoryService;

    /**
     * Before test.
     *
     * @throws Exception the exception
     */
    @SuppressWarnings("unused")
    @Before
    public void beforeTest() throws Exception {
        Category cat = new Category();
        cat.setCatName("cat-test-00");
        cat = categoryService.create(cat);
    }

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    /*
     * (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    /*
     * (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#create(com.hhtank.beverage.domain.Category)}
     * .
     */
    @Test
    public void testCreate() {
        Category cat = new Category();
        cat.setCatName("cat-test-01");
        cat = categoryService.create(cat);
        assertEquals(true, cat.getId() > 0);
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#delete(com.hhtank.beverage.domain.Category)}
     * .
     *
     * @throws ItemNotFoundException the item not found exception
     * @throws DataIntegrityException the data integrity exception
     */
    @Test
    public void testDelete() throws ItemNotFoundException,
    DataIntegrityException {
        int size = categoryService.findAll().size();
        Category cat = categoryService.create(new Category("test-cat-delete-00"));
        categoryService.delete(cat);
        assertEquals(size, categoryService.findAll().size());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#delete(com.hhtank.beverage.domain.Category)}
     * .
     */
    @Test
    public void testDeleteDataIntegrity() {
        Category cat = new Category();
        cat.setCatName("cat-test-01");
        Beer beer = new Beer();
        beer.setBeerName("beer-test-01");
        cat = categoryService.create(cat);
        try {
            categoryService.delete(cat);
        }
        catch (Exception e) {
            assertSame(DataIntegrityException.class, e.getClass());
        }
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#update(com.hhtank.beverage.domain.Category)}
     * .
     */
    @Test
    public void testDeleteNotFound() {
        Category cat = new Category();
        cat.setId(-100);
        cat.setCatName("cat-test-01");
        try {
            categoryService.delete(cat);
        }
        catch (Exception e) {
            assertSame(ItemNotFoundException.class, e.getClass());
        }
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#findAll()}.
     */
    @Test
    public void testFindAll() {
        assertEquals(true, categoryService.findAll().size() > 0);
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#findById(int)}.
     */
    @Test
    public void testFindById() {
        int id = categoryService.findAll().get(0).getId();
        assertNotNull(categoryService.findById(id));
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#mergeOnly(com.hhtank.beverage.domain.Category)}
     * .
     *
     * @throws ItemNotFoundException the item not found exception
     */
    @Test
    public void testMergeOnly() throws ItemNotFoundException {
        Category cat = categoryService.findAll().get(0);
        int id = cat.getId();
        int size = cat.getBeers() == null ? 0 : cat.getBeers().size();
        if (size == 0) {
            cat.setBeers(new HashSet<Beer>());
            cat.getBeers().add(new Beer("beer-test-add-00", cat));
            categoryService.update(cat);
        }
        cat.setBeers(null);
        cat.setCatName("cat-test-01-changed");
        categoryService.mergeOnly(cat);
        Category found = categoryService.findById(id);
        assertEquals("cat-test-01-changed", found.getCatName());
        if (size == 0) {
            assertEquals(1, found.getBeers().size());
        }
        else {
            assertEquals(size, found.getBeers().size());
        }
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#update(com.hhtank.beverage.domain.Category)}
     * .
     *
     * @throws ItemNotFoundException the item not found exception
     */
    @Test
    public void testUpdate() throws ItemNotFoundException {
        Category cat = categoryService.findAll().get(0);
        int id = cat.getId();
        cat.setCatName("cat-test-01-changed");
        categoryService.update(cat);
        Category found = categoryService.findById(id);
        assertEquals("cat-test-01-changed", found.getCatName());

    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.CategoryServiceImpl#update(com.hhtank.beverage.domain.Category)}
     * .
     */
    @Test
    public void testUpdateNotFound() {
        Category cat = new Category();
        cat.setId(-100);
        cat.setCatName("cat-test-01-changed");
        try {
            categoryService.update(cat);
        }
        catch (Exception e) {
            assertSame(ItemNotFoundException.class, e.getClass());
        }
    }
}
