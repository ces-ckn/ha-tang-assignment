/**
 *
 */
package com.hhtank.beverage.service;

import java.util.HashSet;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.hhtank.beverage.domain.User;
import com.hhtank.beverage.domain.UserRole;
import com.hhtank.beverage.enums.EnumRole;
import com.hhtank.beverage.exception.DuplicatedObjectException;
import com.hhtank.beverage.exception.ItemNotFoundException;

/**
 * The test class for UserService.
 *
 * @author hhtank
 */
@RunWith(org.springframework.test.context.junit4.SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:beans-test.xml")
@Transactional
public class UserServiceImplTest
extends TestCase {

    /** The user service. */
    @Autowired
    UserService userService;

    /**
     * Before test.
     *
     * @throws Exception the exception
     */
    @Before
    public void beforeTest() throws Exception {
        User user = new User("user-00", "123");
        user = userService.create(user);
    }

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    /*
     * (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    /*
     * (non-Javadoc)
     * @see junit.framework.TestCase#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.UserServiceImpl#create(com.hhtank.beverage.domain.User)}
     * .
     *
     * @throws DuplicatedObjectException the duplicated object exception
     */
    @Test
    public void testCreate() throws DuplicatedObjectException {
        User user = new User("user-01", "123");
        HashSet<UserRole> roles = new HashSet<UserRole>();
        roles.add(new UserRole(user, EnumRole.ROLE_ADMIN.getValue()));
        user.setUserRoles(roles);
        user.setUserRoles(roles);
        user = userService.create(user);
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.UserServiceImpl#delete(com.hhtank.beverage.domain.User)}
     * .
     *
     * @throws ItemNotFoundException the item not found exception
     */
    @Test
    public void testDelete() throws ItemNotFoundException {
        int size = userService.findAllAdmin().size();
        User user = userService.findAllAdmin().get(0);
        userService.delete(user);
        assertEquals(size - 1, userService.findAllAdmin().size());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.UserServiceImpl#update(com.hhtank.beverage.domain.User)}
     * .
     */
    @Test
    public void testDeleteNotFound() {
        User user = new User("usernotfound", "123");
        try {
            userService.delete(user);
        }
        catch (Exception e) {
            assertSame(ItemNotFoundException.class, e.getClass());
        }
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.UserServiceImpl#findAllAdmin()}.
     */
    @Test
    public void testFindAll() {
        List<User> users = userService.findAllAdmin();
        assertEquals(true, users.size() > 0);
        assertEquals("", users.get(0).getPassword());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.UserServiceImpl#findById(String)}.
     */
    @Test
    public void testFindById() {
        String id = userService.findAllAdmin().get(0).getUsername();
        assertNotNull(userService.findById(id));
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.UserServiceImpl#mergeOnly(com.hhtank.beverage.domain.User)}
     * .
     *
     * @throws ItemNotFoundException the item not found exception
     */
    @Test
    public void testMerge() throws ItemNotFoundException {
        User user = userService.findAllAdmin().get(0);
        String id = user.getUsername();
        int enabled = user.getEnabled();
        user.setPassword("pass-changed");
        user.setEnabled(100);
        userService.mergeOnly(user);
        User found = userService.findById(id);
        assertEquals("pass-changed", found.getPassword());
        assertEquals(enabled, found.getEnabled());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.UserServiceImpl#update(com.hhtank.beverage.domain.User)}
     * .
     *
     * @throws ItemNotFoundException the item not found exception
     */
    @Test
    public void testUpdate() throws ItemNotFoundException {
        User user = userService.findAllAdmin().get(0);
        String id = user.getUsername();
        user.setPassword("pass-changed");
        userService.update(user);
        User found = userService.findById(id);
        assertEquals("pass-changed", found.getPassword());
    }

    /**
     * Test method for
     * {@link com.hhtank.beverage.service.UserServiceImpl#update(com.hhtank.beverage.domain.User)}
     * .
     */
    @Test
    public void testUpdateNotFound() {
        User user = new User("usernotfound", "123");
        try {
            userService.update(user);
        }
        catch (Exception e) {
            assertSame(ItemNotFoundException.class, e.getClass());
        }
    }
}
